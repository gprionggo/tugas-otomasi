#1. Mendefinisikan provider AWS'
provider "aws" {
    region = "us-east-1"
    shared_credentials_file = "~/.aws/credentials.txt"
}

#2. Membuat sebuah VPC
resource "aws_vpc" "vpc_konfigurasi_web" {
  cidr_block = "10.0.1.0/24"
  tags = {
    "Name" = "VPC Konfigurasi Website Statis"
  }
}

#3. security group
#3.1 membuat security group untuk instance web statis
resource "aws_security_group" "sg_web_static" {
  name        = "sg_web_static"
  description = "Security Group Webserver"
  vpc_id      = aws_vpc.vpc_konfigurasi_web.id

    ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG Website Statis"
  }
}

#4. Membuat sebuah Subnet dengan 1 public
resource "aws_subnet" "konfigurasi_web" {
  vpc_id     = aws_vpc.vpc_konfigurasi_web.id
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
  cidr_block = "10.0.1.0/27"

  tags = {
    Name = "Subnet Website Statis"
  }
}

#5. Membuat Internet Gateway Untuk IP Public
resource "aws_internet_gateway" "gateway_web" {
    vpc_id = aws_vpc.vpc_konfigurasi_web.id
    tags = {
      "Name" = "Gateway Website Statis"
    }
}

#6. Membuat route table
resource "aws_route_table" "route_web" {
  vpc_id = aws_vpc.vpc_konfigurasi_web.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway_web.id
  }

  tags = {
    Name = "Routing Table Web Statis"
  }
}

# Membuat route association ke subnet
resource "aws_route_table_association" "route_table_webstatis" {
  subnet_id      = aws_subnet.konfigurasi_web.id
  route_table_id = aws_route_table.route_web.id
}

#6. Membuat EC2 instance
#6.1 Menggunakan Ubntu 20.04
resource "aws_instance" "instance_staging" { 
    ami = "ami-09e67e426f25ce0d7" 
    instance_type = "t2.micro"
    key_name = "tugas_otomasi"
    availability_zone = "us-east-1a"
    subnet_id = aws_subnet.konfigurasi_web.id
    vpc_security_group_ids = [ 
        aws_security_group.sg_web_static.id 
    ]

    tags = {
        Name = "Instance Staging"
   }
}

#6.2 Menggunakan Ubntu 20.04
resource "aws_instance" "instance_production" { 
    ami = "ami-09e67e426f25ce0d7" 
    instance_type = "t2.micro"
    key_name = "tugas_otomasi"
    availability_zone = "us-east-1a"
    subnet_id = aws_subnet.konfigurasi_web.id
    vpc_security_group_ids = [ 
        aws_security_group.sg_web_static.id 
    ]

    tags = {
        Name = "Instance Production"
   }
}